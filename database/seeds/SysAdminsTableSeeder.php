<?php

use Illuminate\Database\Seeder;

class SysAdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\SysAdmin::class, 1)->create();
    }
}
