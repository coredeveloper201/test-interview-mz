<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\SysAdmin::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        // 'email' => $faker->unique()->safeEmail,
        'email' => 'sysadmin@admin.com',
        'role' => 'SysAdmin',
        'email_verified_at' => now(),
        'password' => '$2y$10$J1DPXryPP9aJBguz8o0kh.D8hLlvTRDZ1G/la/b27OE.6mpFwOMCe', // admin321
        'remember_token' => str_random(10),
    ];
});
