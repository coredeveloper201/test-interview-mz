<?php

namespace App\Policies;

use App\Models\Admin;
// use App\Models\SysAdmin;
// use App\Models\Manager;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdminPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if a user is authorize for a role.
     *
     * @param \App\Models\Admin $user
     * @return bool
     */
    public function authorize(Admin $user)
    {
        return $user->role;
    }
}
