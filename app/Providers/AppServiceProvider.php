<?php

namespace App\Providers;

use Faker\Factory as FakerFactory;
use Faker\Generator as FakerGenerator;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        /* Setting the localization option in app's config for Faker */
        // Just commented: laravel doesn't actually make use of env() anywhere in its service providers, so maybe a config option would be better
/*        $this->app->singleton(FakerGenerator::class, function () {
          return FakerFactory::create(env('FAKER_LOCALE', 'en_US'));
        });*/
       // tell Faker to use the app's faker locale
        $this->app->singleton(FakerGenerator::class, function () {
            return FakerFactory::create(config('app.faker_locale'));
        });

    }
}
