@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            <div class="card card-new-transaction mb-4">
                <div class="card-header">New loan form</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('loans.rate') }}" class="needs-validation">
                        @csrf
                        <div class="form-row align-items-center">
                            <div class="form-group col-md-8">
                                {{-- <label for="amount">Amount</label> --}}
                                  <div class="input-group">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="amoutGroupPrepend">৳</span>
                                    </div>
                                    <input type="number" pattern="\d+" title='Only Number'  value="{{ old('amount') }}" min="0" step="1.00" data-number-to-fixed="2" data-number-stepfactor="100"  class="form-control{{ $errors->has('amount') ? ' is-invalid' : '' }} currency text-right" id="amount" name="amount" placeholder="Enter your loan amount from Tk.1,000 to Tk.20,000" aria-describedby="amoutGroupPrepend"  />
                                    @if ($errors->has('amount'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('amount') }}</strong>
                                        </span>
                                    @endif
                                  </div>                              
                            </div>    
                            <div class="form-group col-md-8">
                                {{-- {{ dd($errors) }} --}}
                                {{-- <label for="purpose">Purpose
                                </label> --}}
                                <select name="purpose" id="purpose" class="form-control{{ $errors->has('purpose') ? ' is-invalid' : '' }} ">
                                  <option value="" selected disabled>Choose your purpose</option>
                                  <option value="1" {{ old('purpose') =='1' ? ' selected="selected" ' : '' }} >Home improvement</option>
                                  <option value="2" {{ old('purpose') =='2' ? ' selected="selected" ' : '' }} >Personal Loan</option>
                                  <option value="3" {{ old('purpose') =='3' ? ' selected="selected" ' : '' }} >Car Loan</option>
                                </select>
                                            
                                @if ($errors->has('purpose'))
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $errors->first('purpose') }}</strong>
                                    </span>
                                @endif
                            </div>    
                            <div class="form-group col-md-8">
                                {{-- {{ dd($errors) }} --}}
                                {{-- <label for="term">Purpose
                                </label> --}}
                                <select name="term" id="term" class="form-control{{ $errors->has('term') ? ' is-invalid' : '' }} ">
                                  <option value="" selected disabled>Choose your term</option>
                                  <option value="1" {{ old('term') =='1' ? ' selected="selected" ' : '' }} >1 Year</option>
                                  <option value="2" {{ old('term') =='2' ? ' selected="selected" ' : '' }} >2 Year</option>
                                  <option value="3" {{ old('term') =='3' ? ' selected="selected" ' : '' }} >3 year</option>
                                </select>
                                            
                                @if ($errors->has('term'))
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $errors->first('term') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-md-8">
                              <div class="input-group date" data-provide="datepicker">
                                  <input type="text" name="start_at" class="form-control{{ $errors->has('start_at') ? ' is-invalid' : '' }} datepicker" value="{{ old('start_at') }}">
                                  <div class="input-group-addon">
                                      <span class="glyphicon glyphicon-th"></span>
                                  </div>
                              </div>

                                @if ($errors->has('start_at'))
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $errors->first('start_at') }}</strong>
                                    </span>
                                @endif
                          </div>
                            
                            <div class="form-group col-md-8 text-right mb-0">
                                <button type="submit" class="btn btn-primary">Check your rate</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
             @if(session()->has('data'))
         
            {{-- {{ dd(session()->get('data')) }} --}}
              <form method="POST" action="{{ route('loans.store') }}">
               @csrf
               

             @foreach(session()->get('data') as $inputName => $inputValue)

              <input type="hidden" name="{{ $inputName }}" value="{{ $inputValue }}">

             @endforeach
             
            <div class="card">
                <div class="card-header">Save Database</div>

                <div class="card-body">

                      
                      <dl class="row">
                        <dt class="col-sm-4">Loan Amount</dt>
                        <dd class="col-sm-8">{{ session()->get('data')['loanAmount'] }} </dd>
                        <dt class="col-sm-4">Interest Rate</dt>
                        <dd class="col-sm-8">{{ session()->get('data')['interestRate'] }} </dd>
                        <dt class="col-sm-4">Purpose</dt>
                        <dd class="col-sm-8">{{ session()->get('data')['purposeDesc'] }} </dd>
                        <dt class="col-sm-4">Monthly Payment</dt>
                        <dd class="col-sm-8">{{ 'Tk. ' . session()->get('data')['monthlyPayment'] . ' ( ' . session()->get('data')['numberOfPaymentInMonth'] . ' Payments )' }} </dd>
                        <dt class="col-sm-4">Start Date</dt>
                        <dd class="col-sm-8">{{ session()->get('data')['startDate'] }} </dd>
                        <dt class="col-sm-4">Total Interest Paid</dt>
                        <dd class="col-sm-8">{{ 'Tk. ' . session()->get('data')['totalInterestPaid'] }} </dd>
                        <dt class="col-sm-4">Last Payment </dt>
                        <dd class="col-sm-8">{{ session()->get('data')['lastPaymentDate'] }} </dd>
                      </dl>

                   <button type="submit" class="btn btn-primary">Get Loan</button>
                    
                </div>
            </div>
                   </form>
                   @endif
        </div>
    </div>
</div>
@endsection