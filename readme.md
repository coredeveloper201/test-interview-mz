## Steps

1. Run composer update on terminal
2. just duplicate the 'create_users_table' migration file for admin table but remember to make the necessary modifications
3. Run php artisan migrate
4. Create and run session and cache table
5. Setup the config and env settings option for SESSION_DOMAIN
6. run composer require laravel/passport
7. Run php artisan make:auth and update related files
8. Run php artisan vendor publish
9. Run php artisan passport:install
10. just duplicate the 'create_users_table' migration file for customer table but remember to make the necessary modifications
11. Run php artisan migrate
12. Create an run migration to add role column to admins table
13. Setup route middleware for passport API
14. Create and setup for auth guards, providers and related models 
15. Setup and run php artisan db seed for System Admin
16. Just commented for SysAdminsTableSeeder on DatabaseSeeder file after seed
17. Add role policy for admin users
18. Update record for sysadmin credential as user: sysadmin@admin.com and password: admin321


